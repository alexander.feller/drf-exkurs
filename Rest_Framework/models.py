from django.contrib.auth.models import User
from django.db import models


class Calendar(models.Model):
    title = models.CharField(max_length=100, blank=False, null=False)
    description = models.TextField(max_length=10000, null=False)
    start_date = models.DateTimeField(blank=False)
    end_date = models.DateTimeField(blank=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
