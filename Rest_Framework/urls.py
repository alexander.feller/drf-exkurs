from django.urls import path, include
from rest_framework.routers import DefaultRouter

from Rest_Framework import views

router = DefaultRouter()
router.register(r'user', views.UserViewSet)

urlpatterns = [
    # Base URL = api/v1/
    path('', include(router.urls)),
    path('calendar', views.CalendarList.as_view(), name='CalendarList'),
    path('calendar/<int:pk>', views.CalendarDetails.as_view(), name='CalendarDetails')
]
