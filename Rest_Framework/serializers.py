from django.contrib.auth.models import User
from rest_framework import serializers, status

from Rest_Framework.models import Calendar


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User

        extra_kwargs = {'password': {'write_only': True}}
        fields = '__all__'

    def create(self, validated_data):
        user = User.objects.create(username=validated_data['username'])
        user.set_password(validated_data['password'])
        user.save()
        return user


class CalendarSerializer(serializers.ModelSerializer):
    class Meta:
        model = Calendar
        extra_kwargs = {'title': {'required': True}, 'description': {'required': True},
                        'start_date': {'required': True}, 'end_date': {'required': True}}
        exclude = ('user',)

    def create(self, validated_data):
        calendar_entry = Calendar.objects.create(
            title=validated_data['title'],
            description=validated_data['description'],
            start_date=validated_data['start_date'],
            end_date=validated_data['end_date'],
            user=self.context['request'].user
        )
        return calendar_entry

    def update(self, instance, validated_data):
        user = self.context['request'].user
        if user != instance.user:
            raise serializers.ValidationError({"Error": "You do not have the right permissions"},
                                              status.HTTP_400_BAD_REQUEST)
        if 'title' in validated_data:
            instance.title = validated_data['title']
        if 'description' in validated_data:
            instance.description = validated_data['description']
        if 'start_date' in validated_data:
            instance.start_date = validated_data['start_date']
        if 'end_date' in validated_data:
            instance.end_date = validated_data['end_date']
        instance.save()
        return instance
