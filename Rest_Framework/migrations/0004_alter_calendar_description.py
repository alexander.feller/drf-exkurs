# Generated by Django 3.2.6 on 2021-10-07 12:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Rest_Framework', '0003_auto_20210928_1357'),
    ]

    operations = [
        migrations.AlterField(
            model_name='calendar',
            name='description',
            field=models.TextField(blank=True, max_length=10000, null=True),
        ),
    ]
