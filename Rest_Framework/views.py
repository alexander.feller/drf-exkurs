from django.contrib.auth.models import User
from django.http import Http404
from rest_framework import status, permissions, viewsets
from rest_framework.response import Response
from rest_framework.views import APIView

from Rest_Framework.models import Calendar
from Rest_Framework.serializers import CalendarSerializer, UserSerializer


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class CalendarList(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request):
        calendar_entries = Calendar.objects.filter(user=request.user)
        serializer = CalendarSerializer(calendar_entries, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = CalendarSerializer(data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CalendarDetails(APIView):
    permission_classes = [permissions.IsAuthenticated]

    @staticmethod
    def get_calendar_entry(pk):
        try:
            return Calendar.objects.get(pk=pk)
        except Calendar.DoesNotExist:
            raise Http404

    def get_calendar_entry_with_permission(self, request, pk):
        calendar_entry = CalendarDetails.get_calendar_entry(pk)
        if request.user != calendar_entry.user:
            return Response({"Error": "Forbidden"}, status=status.HTTP_403_FORBIDDEN)
        return calendar_entry

    def get(self, request, pk):
        calendar_entry = CalendarDetails.get_calendar_entry_with_permission(self, request, pk)
        serializer = CalendarSerializer(calendar_entry)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def patch(self, request, pk):
        calendar_entry = CalendarDetails.get_calendar_entry_with_permission(self, request, pk)
        serializer = CalendarSerializer(calendar_entry, data=request.data, context={'request': request}, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status.HTTP_200_OK)
        return Response({"Error": "Invalid data"}, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        calendar_entry = CalendarDetails.get_calendar_entry_with_permission(self, request, pk)
        calendar_entry.delete()
        return Response({"Success": "Calendar entry successfully deleted"}, status=status.HTTP_204_NO_CONTENT)
